---
title: Update coreboot on Thinkpad X220
date: 2020-09-25
---

git pull
make nconfig
delete old /build
delete all folders in 3rdparty except blobs (ME already neutralized)
dont change location of vga bios / bootsplash img
make
sudo flashrom -p internal:laptop=force_I_want_a_brick -c MX25L6405 -w build/coreboot.rom